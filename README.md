# simple_robot_world

A simple ROS robot in a 2D world

## Getting started
``` shell
cd <catkin workspace>/src
git clone https://gitlab.com/delmann/simple_robot_world
cd ..
catkin init
catkin build
roslaunch simple_robot_world simple_robot_world.launch
```

## Example

<img src="https://gitlab.com/delmann/simple_robot_world/-/raw/main/.resources/turtle_ui.gif"/>