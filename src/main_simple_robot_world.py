#!/usr/bin/env python

import rospy
from std_msgs.msg import String

from simple_robot_world.msg import PointPose2D

import random

FRAME_SIZE = 25 
ROBOT_RADIUS = 0.5
RATE = 10
TARGET_TOPIC = 'target_topic'

class SimpleRobot():

	def __init__(self, rate, radius, point_pose):
		self.rate = rate
		self.radius = radius
		self.point_pose = point_pose

		# Target point subscriber
		self.target_topic = TARGET_TOPIC
		self.sub = rospy.Subscriber(self.target_topic, PointPose2D, self.point_pose_callback)

	def point_pose_callback(data):
		rospy.loginfo("%s" % data)

class RobotController():

	def __init__(self, rate, point_pose):
		self.rate = rate

		# Target point publisher
		self.target_topic = TARGET_TOPIC
		self.point_pose = point_pose

		self.pub = rospy.Publisher(self.target_topic, PointPose2D, queue_size = 100)

	def run_robot_controller(self):
		while not rospy.is_shutdown():
			rospy.loginfo(self.point_pose)
			self.pub.publish(self.point_pose)
			self.rate.sleep()

def add_random_point():
	point_pose = PointPose2D()
	point_pose.x = random.random()
	point_pose.y = random.random()	
	return point_pose

if __name__ == '__main__':
	
	try:
		rospy.loginfo("simple robot script started")

		# Init node
		node_name = 'simple_robot'
		rospy.init_node(node_name, anonymous = True)
		rate = rospy.Rate(RATE) 

		point_pose = add_random_point()

		simple_robot_controller = RobotController(rate, point_pose)
		simple_robot = SimpleRobot(rate, ROBOT_RADIUS, point_pose)
		simple_robot_controller.run_robot_controller()
		
		rospy.spin()

	except rospy.ROSInterruptException:
		rospy.logwarn("something went wrong!")
		rospy.logerror("simple robot script failed!")
		pass
	
	finally:
		rospy.loginfo("simple robot script finished")