#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from simple_robot_world.msg import PointPose2D
from simple_robot_world.msg import PointVel2D

import random
from math import sqrt

import turtle

class SimpleWorld(object):

	def __init__(self, frame_size = 25, 
		robot_radius = 5, max_vel = 5, pose_tolerance = 0.05, slow_down_distance = 1.5 ):

		# Topic names
		self.target_topic = 'target_point'
		self.robot_pose_topic = 'robot_pose'
		self.robot_vel_topic = 'robot_vel'
		self.nominal_vel_topic = 'nominal_vel'

		# Msg instantiation
		self.robot_pose= PointPose2D()
		self.robot_vel = PointVel2D()
		self.point_pose = PointPose2D()
		self.robot_nominal_vel = PointVel2D()

		# Init world parameters
		self.frame_size = frame_size
		self.robot_radius = robot_radius
		self.collision_frame = self.frame_size - self.robot_radius
		self.max_vel = max_vel
		self.pose_tolerance = pose_tolerance
		self.slow_down_distance = slow_down_distance

	def add_random_point(self, max_distance):
		point_pose = PointPose2D()
		point_pose.x = random.random()*max_distance
		point_pose.y = random.random()*max_distance
		return point_pose

	def add_random_point_list(self, length, max_distance):
		point_pose_list = PointPose2DArray()
		point_pose_list.random_array(length, max_distance)
		return point_pose_list

	def calculate_position_diff(self, pose1, pose2):
		diff_vector = PointPose2D()
		diff_vector.x = pose1.x - pose2.x
		diff_vector.y = pose1.y - pose2.y
		return diff_vector

	def euclidian_distance(self, vector):
		dist = sqrt(vector.x**2 + vector.y**2)
		return dist

	def normalize_pose_vector(self, vector):
		vector_magnitude = self.euclidian_distance(vector)
		normalized_vector = PointPose2D()
		normalized_vector.x = vector.x/vector_magnitude
		normalized_vector.y = vector.y/vector_magnitude 
		return normalized_vector

class PointPose2DArray():

	def __init__(self):
		self.x_pose = list()
		self.y_pose = list()

	def random_array(self, n, max_val):
		for i in range(n):
			self.x_pose.append(random.random()*max_val)
			self.y_pose.append(random.random()*max_val)

	def read_i_pose(self, i):
		try:
			point_pose = PointPose2D()
			point_pose.x = self.x_pose[i]
			point_pose.y = self.y_pose[i]
		except IndexError:
			point_pose = 'Null'
			pass
		return point_pose

	def get_length(self):
		return len(self.x_pose)

	def print_array(self):
		for i in range(self.get_length()):
			print(self.x_pose[i])
			print(self.y_pose[i])
