#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from simple_robot_world.msg import PointPose2D
from simple_robot_world.msg import PointVel2D

from simple_world import SimpleWorld

import turtle
class SimpleRobot(SimpleWorld):

	def __init__(self, node_name = 'simple_robot', rate = 10):
		SimpleWorld.__init__(self)

		# Init custom node
		self.node_name = node_name
		rospy.init_node(self.node_name, anonymous = True)
		self.rate = rospy.Rate(rate) 

		# Target point and velocity subscribers
		self.sub_point_pose = rospy.Subscriber(self.target_topic, PointPose2D, self.callback_point_vector)
		self.sub_nominal_vel = rospy.Subscriber(self.nominal_vel_topic, PointVel2D, self.callback_vel_vector)

		# Init time
		self.time = rospy.get_time()

		# Publishers
		self.pub_robot_pose = rospy.Publisher(self.robot_pose_topic, PointPose2D, queue_size = 100)
		self.pub_robot_vel = rospy.Publisher(self.robot_vel_topic, PointVel2D, queue_size = 100)
		self.pub_time = rospy.Publisher('ros_time', String, queue_size = 100)

		# Init robot pose
		self.robot_pose = PointPose2D()
		self.robot_pose.x = 0
		self.robot_pose.y = 0

		# turtle world
		self.robot_world = turtle.Screen()
		self.robot_world.title('simple_robot')
		self.robot_world.bgcolor('black')
		self.turtle_ui_scale = 15
		self.canvas_size = self.frame_size*self.turtle_ui_scale
		self.robot_world.screensize(self.canvas_size, self.canvas_size)

		# turtle robot
		self.robot_sim = turtle.Turtle()
		self.robot_sim.shape('circle')
		self.robot_sim.color('green')
		self.robot_sim.penup()

		# turtle points
		self.point = turtle.Turtle()
		self.point.shape('circle')
		self.point.color('white')

	def callback_point_vector(self, data):
		self.point_pose = data
		rospy.loginfo("%s" % data)

	def callback_vel_vector(self, data):
		self.robot_nominal_vel = data
		rospy.loginfo("%s" % data)

	def update_velocity(self):
		self.robot_vel.vel_x = self.robot_nominal_vel.vel_x 
		self.robot_vel.vel_y = self.robot_nominal_vel.vel_y

	def update_position(self, temp_robot_pose, before):
		now = rospy.get_time()
		time = now-before
		self.robot_pose.x = temp_robot_pose.x + self.robot_vel.vel_x*time
		self.robot_pose.y = temp_robot_pose.y + self.robot_vel.vel_y*time

	def update_turtle(self):
		self.robot_sim.dx = self.robot_vel.vel_x*self.turtle_ui_scale
		self.robot_sim.dy = self.robot_vel.vel_y*self.turtle_ui_scale
		self.robot_sim.setpos(self.robot_pose.x*self.turtle_ui_scale, 
			self.robot_pose.y*self.turtle_ui_scale)

	def update_point(self):
		self.point.setpos(self.point_pose.x*self.turtle_ui_scale, 
			self.point_pose.y*self.turtle_ui_scale)

	def got_2_point(self):

		temp_robot_pose = PointPose2D()
		temp_robot_pose.x = self.robot_pose.x
		temp_robot_pose.y = self.robot_pose.y

		while not rospy.is_shutdown():

			# Actuate robot
			self.update_point()
			self.update_velocity()
			self.update_position(temp_robot_pose, self.time)
			self.update_turtle()
			temp_robot_pose.x = self.robot_pose.x
			temp_robot_pose.y = self.robot_pose.y
			self.time = rospy.get_time()

			# Publish robot update
			self.pub_robot_pose.publish(self.robot_pose)
			self.pub_robot_vel.publish(self.robot_vel)
			self.pub_time.publish(str(self.time))
			self.rate.sleep()

if __name__ == '__main__':
	
	try:
		simple_robot = SimpleRobot()
		simple_robot.got_2_point()
		
	except rospy.ROSInterruptException:
		rospy.logwarn("something went wrong!")
		rospy.logerror("simple robot script failed!")
		pass
	
	finally:
		rospy.loginfo("simple robot script finished")