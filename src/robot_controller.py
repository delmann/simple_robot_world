#!/usr/bin/env python

import rospy
from simple_world import SimpleWorld
from std_msgs.msg import String
from simple_robot_world.msg import PointPose2D
from simple_robot_world.msg import PointVel2D

from math import sqrt
import random

class RobotController(SimpleWorld):

	def __init__(self, node_name = 'robot_controller',rate = 10):
		SimpleWorld.__init__(self)

		# Init custom node
		self.node_name = node_name
		rospy.init_node(self.node_name, anonymous = True)
		self.rate = rospy.Rate(rate) 

		# Start Publishers
		self.pub_point_pose = rospy.Publisher(self.target_topic, PointPose2D, queue_size = 100)
		self.pub_nominal_vel = rospy.Publisher(self.nominal_vel_topic ,PointVel2D, queue_size = 100)
		self.pub_pose_diff = rospy.Publisher('pose_diff' ,PointPose2D, queue_size = 100)

		# Start Subscribers
		self.sub_pose = rospy.Subscriber(self.robot_pose_topic, PointPose2D, self.callback_pose_vector)
		self.sub_vel = rospy.Subscriber(self.robot_vel_topic, PointVel2D, self.callback_vel_vector)

	def callback_pose_vector(self, data):
		self.robot_pose = data
		rospy.loginfo("%s" % data)

	def callback_vel_vector(self, data):
		self.robot_vel = data
		rospy.loginfo("%s" % data)

	def fake_controller(self, diff_vector):
		vel_vector = PointVel2D()	
		diff_vector_magnitude = self.euclidian_distance(diff_vector)

		if (diff_vector_magnitude <= self.pose_tolerance):
			vel_vector.vel_x = 0
			vel_vector.vel_y = 0

		elif (diff_vector_magnitude <= self.slow_down_distance):
			normalized_diff_vector = self.normalize_pose_vector(diff_vector)
			slow_down_factor = self.max_vel * diff_vector_magnitude * 0.5
			vel_vector.vel_x = normalized_diff_vector.x*slow_down_factor
			vel_vector.vel_y = normalized_diff_vector.y*slow_down_factor

		else:
			normalized_diff_vector = self.normalize_pose_vector(diff_vector)
			vel_vector.vel_x = normalized_diff_vector.x*self.max_vel
			vel_vector.vel_y = normalized_diff_vector.y*self.max_vel

		return vel_vector

	def send_vel_2_point(self, point_pose):

		while (self.calculate_position_diff(point_pose, self.robot_pose) >= self.pose_tolerance):

			pos_diff = self.calculate_position_diff(point_pose, self.robot_pose)
			nominal_vel = self.fake_controller(pos_diff)

			self.pub_pose_diff.publish(pos_diff)
			self.pub_nominal_vel.publish(nominal_vel)
			self.pub_point_pose.publish(point_pose)

			self.rate.sleep()

	def send_vel_2_point_list(self, point_pose_list):

		i = 0
		while not rospy.is_shutdown():
			
			point_pose = point_pose_list.read_i_pose(i)
			robot_controller.pub_point_pose.publish(point_pose)
			pos_diff = self.calculate_position_diff(point_pose, self.robot_pose)

			while (pos_diff >= self.pose_tolerance):

				pos_diff = self.calculate_position_diff(point_pose, self.robot_pose)
				nominal_vel = self.fake_controller(pos_diff)
				self.pub_pose_diff.publish(pos_diff)
				self.pub_nominal_vel.publish(nominal_vel)
				self.pub_point_pose.publish(point_pose)

				if ((nominal_vel.vel_x + nominal_vel.vel_y) == 0):
					i = i + 1
					point_pose = point_pose_list.read_i_pose(i)
					robot_controller.pub_point_pose.publish(point_pose)
				
				self.rate.sleep()

			self.rate.sleep()


if __name__ == '__main__':
	
	try:
		robot_controller = RobotController()
		num_of_points = 100

		point_pose_list = robot_controller.add_random_point_list(num_of_points, robot_controller.frame_size)
		print(point_pose_list.get_length())
		robot_controller.send_vel_2_point_list(point_pose_list)

	except rospy.ROSInterruptException:
		rospy.logerror("robot controller failed!")
		pass
	
	finally:
		rospy.loginfo("simple robot script finished")